# README #

Javaの勉強をかねたサンプル集です.

原則, openjdkを使っています.

	% java -version
	openjdk version "1.8.0_92"
	OpenJDK Runtime Environment (build 1.8.0_92-b14)
	OpenJDK 64-Bit Server VM (build 25.92-b14, mixed mode)
	%

もうひとつ原則として, コードの結果や成否はテストコードで示すようにします.
